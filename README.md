Chess-Lib
=========

## Development

### Init

Enable the git pre-commit hook using:
`chmod +x .githooks/pre-commit && git config --local core.hooksPath`

### Testing

To run all test use `cargo test`. This will also run the more expensive tests.
During development you can use `cargo test --no-default-features` to run only the faster tests.

### Code Generation
Generate magic entries: `cargo run --release chess generate > /tmp/magics_entries.rs && mv /tmp/magics_entries.rs src/engine/chess/magics_entries.rs`

### Debugging

#### Profiling

For an initial look we can create a flamegraph using `cargo-flamegraph` on Linux by running: `cargo build --release && flamegraph -o my_flamegraph.svg -- target/release/chess-lib chess perft --fen "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -" --depth 5`

For profiling we can use `perf` on Linux. Run the perft command with perf record and profile until it exits:
`cargo build --release && perf record -F99 --call-graph dwarf target/release/chess-lib chess perft --fen "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -" --depth 5`

This will output a file called `perf.data`.

To view the profiling results you use `perf report` and to view a specific data file you can use `perf report -i <path-to-perf-data-file>`.

Or to compare to reports you can use `perf diff <path-to-base-line-perf-data-file> <path-to-new-perf-data-file>`

##### Stockfish & Perft

###### Stockfish

Download [Stockfish](https://github.com/official-stockfish/Stockfish) and compile it using `make -j build ARCH=x86-64-modern`. Then run `./stockfish`

You can enter a specific FEN using e.g. `position fen rnbqkbnr/1ppppppp/p7/8/8/7P/PPPPPPP1/RNBQKBNR w KQkq - 0 1`

You can also visualize this by entering `d`

Then to run perft with a specific depth (e.g. 3) enter `go perft 3`

###### Perft

Run eg. `cargo run --release chess perft --fen "rnbqkbnr/1ppppppp/p7/8/8/7P/PPPPPPP1/RNBQKBNR w KQkq - 0 1" --depth 3

###### Debugging

First compile a release build doing `cargo build --release`

There is a debugging script which can be run using this command: `python perft.py <path-to-stockfish-executable> <path-to-rechess-engine-executable> "<fen>" <depth>`

### Publishing

To publish a new version of the crate you need to do the following:

1. Update the version in `Cargo.toml` for `version` and `documentation`
2. Run `cargo test` to ensure all tests pass
3. Inspect the generate documentation by running `cargo doc --no-deps --open`
4. Git commit the changes
5. Run `cargo publish --dry-run` to ensure there aren’t any warnings or errors before publishing.
6. Inspect the generated .crate by running `cargo package --list`
7. Run `cargo publish` to publish the crate to crates.io
