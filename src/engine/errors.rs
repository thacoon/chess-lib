use std::fmt;

#[derive(Debug)]
pub enum ChessError {
    ValueError(String),
    OtherError(String),
}

impl fmt::Display for ChessError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ChessError::ValueError(msg) => write!(f, "Value error: {}", msg),
            ChessError::OtherError(msg) => write!(f, "Other error: {}", msg),
        }
    }
}
