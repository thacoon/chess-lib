mod bitboard;
pub mod board;
pub mod common;
mod errors;
mod fen;
pub mod magics;
mod magics_entries;
pub mod move_generator;
mod moves;
