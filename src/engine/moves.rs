use crate::engine::bitboard::{occupied_squares, Bitboard, A_FILE, B_FILE, G_FILE, H_FILE};
use crate::engine::common::{Move, Square};
use crate::engine::magics::{magic_index_with_offset, BishopTable, RookTable, Tables};
use crate::engine::magics_entries::{BISHOP_MAGICS, ROOK_MAGICS};

pub fn to_moves(bitboard: Bitboard, src: Square) -> Vec<Move> {
    let occupied = occupied_squares(bitboard);

    let mut result = Vec::with_capacity(occupied.len());

    for &dst in occupied.iter() {
        result.push(Move::new(src, dst, None));
    }

    result
}

pub fn lookup_king(king: Bitboard) -> Bitboard {
    let pseudo_moves: Bitboard = (king & !H_FILE) << 9
        | king << 8
        | (king & !A_FILE) << 7
        | (king & !H_FILE) << 1
        | (king & !A_FILE) >> 1
        | (king & !H_FILE) >> 7
        | king >> 8
        | (king & !A_FILE) >> 9;

    pseudo_moves
}

pub fn lookup_knight(knights: Bitboard) -> Bitboard {
    (knights & !H_FILE) << 17
        | (knights & !A_FILE) << 15
        | (knights & !(G_FILE | H_FILE)) << 10
        | (knights & !(A_FILE | B_FILE)) << 6
        | (knights & !(G_FILE | H_FILE)) >> 6
        | (knights & !(A_FILE | B_FILE)) >> 10
        | (knights & !H_FILE) >> 15
        | (knights & !(A_FILE)) >> 17
}

pub fn lookup_queen(
    tables: &Tables,
    queen: Square,
    same_color: Bitboard,
    occupied: Bitboard,
) -> Bitboard {
    lookup_bishop(&tables.bishop, queen, same_color, occupied)
        | lookup_rook(&tables.rook, queen, same_color, occupied)
}

pub fn lookup_bishop(
    bishop_table: &BishopTable,
    bishop: Square,
    same_color: Bitboard,
    occupied: Bitboard,
) -> Bitboard {
    let magic = BISHOP_MAGICS[bishop.index() as usize];
    *bishop_table
        .get(magic_index_with_offset(&magic, occupied))
        .unwrap()
        & !same_color
}

pub fn lookup_rook(
    rook_table: &RookTable,
    rook: Square,
    same_color: Bitboard,
    occupied: Bitboard,
) -> Bitboard {
    let magic = ROOK_MAGICS[rook.index() as usize];
    *rook_table
        .get(magic_index_with_offset(&magic, occupied))
        .unwrap()
        & !same_color
}
